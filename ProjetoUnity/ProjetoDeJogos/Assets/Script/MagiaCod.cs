﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagiaCod : MonoBehaviour
{
    public Material Magias1, Magias2;
    public Vector2 PosPedra;
    public bool PedraPodeCair;
    public Vector3 posicaoMouse;
    public Vector3 enPos;
    public LineRenderer lRender;
    public GameObject Player;
    public float TempoAtual, TempoDoTurbilhao,tempoEsfera,velMagia, laserLenght;
    Rigidbody2D rb;
    public int dano, tipoElemento, tipoMagia;
    public List<Sprite> magiasEsfera = new List<Sprite>();

    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");


        rb = GetComponent<Rigidbody2D>();

        tipoElemento = PlayerAtack.tipoElemento;
        tipoMagia = PlayerAtack.tipoMagia;
        if (tipoMagia == 1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().material = Magias1;
        }
        else if (tipoMagia==2)
        {
            this.gameObject.GetComponent<SpriteRenderer>().material = Magias2;
        }
        


            switch (tipoMagia)
            {
           
                case 1:
                if (tipoElemento == 1)
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                      dano = 2; 
                    this.GetComponent<SpriteRenderer>().color = Color.white;
                    this.GetComponent<CircleCollider2D>().radius = 0.15f;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[0];
                    TempoAtual = tempoEsfera;
                    break;
                } else if (tipoElemento == 2)
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                    velMagia = 150;
                    dano = 3;
                  //  this.GetComponent<SpriteRenderer>().color = Color.white;
                    this.GetComponent<CircleCollider2D>().radius = 0.15f;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[1];
                    TempoAtual = tempoEsfera;
                    break;
                } else if (tipoElemento ==3)
                {
                    this.transform.localScale = new Vector3(5, 5, 1);
                    velMagia = 0.2f;
                    this.GetComponent<CircleCollider2D>().radius = 0.75f;
                   // this.GetComponent<SpriteRenderer>().color = Color.blue;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[2];
                    break;
                }
                else if (tipoElemento == 4)
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                    rb.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[3];
                    // this.GetComponent<SpriteRenderer>().color = Color.white;
                    rb.gravityScale = 1;

                    rb.inertia = 5;
                    Debug.Log("entrou no start pedra");
                    PedraPodeCair = true;
                   
                    velMagia = 21f;
                    this.GetComponent<CircleCollider2D>().radius = 0.15f;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[3];
                   
                    break;
                }
                break;
                case 2:
                if (tipoElemento == 1)
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                   // this.GetComponent<SpriteRenderer>().color = Color.white;
                    dano = 2;
                    this.GetComponent<CircleCollider2D>().radius = 0.75f;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[4];
                    TempoAtual = TempoDoTurbilhao;
                    break;
                }
                else if (tipoElemento == 2)
                {
                   // this.GetComponent<SpriteRenderer>().color = Color.white;
                    dano = 3;
                    this.GetComponent<CircleCollider2D>().radius = 0.75f;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[5];
                    //this.GetComponent<SpriteRenderer>().color = Color.red;
                    this.transform.localScale = new Vector3(3, 2, 1);
                    TempoAtual = tempoEsfera;

                    break;
                }
                else if (tipoElemento == 3)
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                    velMagia = 10f;
                    dano = 5;
                    this.GetComponent<CircleCollider2D>().radius = 0.75f;
                   // this.GetComponent<SpriteRenderer>().color = Color.blue;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[6];
                    break;
                }
                else if (tipoElemento ==4)
                {
                   // this.GetComponent<SpriteRenderer>().color = Color.white;
                    dano = 6;
                    velMagia = 20f;
                    this.transform.localScale = new Vector3(1,1,1);
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[7];

                    break;
                }
                    break;
            }
          
    }
    private void Update()
    {
        if (tipoElemento == 3 && tipoMagia == 2)
        {
            Bolha();

            if (Input.GetMouseButtonUp(0))
            {
                Destroy(this.gameObject);
            }
        }
        if (tipoElemento == 2 && tipoMagia == 2 && TempoAtual <= 0 )
        {

            Destroy(this.gameObject);
        }
    }
    private void FixedUpdate()
    {
     
        //destroi as magias
        if (tipoElemento == 1 && tipoMagia == 2 && TempoAtual <= 0)
        {
            Destroy(this.gameObject);
        }
        if (tipoElemento == 2 && tipoMagia == 2 && TempoAtual <= 0)
        {
            
            Destroy(this.gameObject);
        }

        if (TempoAtual >= 0)
        {
            TempoAtual -= Time.deltaTime;
        }
        else
        {
            //tempoDaMagiaTazerado
        }
        if ((tipoMagia == 2 && tipoElemento == 2))
        {
            if (Input.GetMouseButtonUp(0))
            {
                Destroy(this.gameObject);
            }

        }
        if (tipoElemento == 4 && tipoMagia == 1)
        {
            if (transform.position.x != posicaoMouse.x && PedraPodeCair)
            {

                transform.position = Vector3.MoveTowards(this.transform.position, posicaoMouse, velMagia * Time.deltaTime);
            }
            else
            {

                PedraPodeCair = false;
            }

        }
        //magia de fogo 1
        if (tipoElemento == 2 && tipoMagia == 1)
        {

            rb.velocity = Vector2.right * Time.deltaTime * velMagia;
            FogoOrbita();
        }
    }

    public void FogoOrbita()
    {
        
        this.transform.position = new Vector3(this.transform.position.x ,Player.transform.position.y,this.transform.position.z);
        transform.RotateAround(Player.transform.position,Vector3.down,velMagia*Time.deltaTime);
    }
    public void EsferaAr(Transform Pos)
    {
        this.GetComponent<Rigidbody2D>().AddForce( 100*Pos.right * velMagia*Time.deltaTime, ForceMode2D.Impulse);
        Destroy(this.gameObject, 3);
    }
    public void ImpulsoAr()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Debug.Log("Entru na funcao do pulo");
        //20 numero magico
        Player.GetComponent<Rigidbody2D>().velocity = Vector2.up * 20;
    }
    public void TurbilhaodeAr()
    {
        this.transform.localScale = Vector3.one * 3;
        //animacao
    }
    public void SoproDoDragao()
    {
        //animacao
    }
    public void Pedrada(Vector3 posMouse)
    {
        posicaoMouse = posMouse;
        transform.position = Vector3.MoveTowards(this.transform.position,posicaoMouse, velMagia * Time.deltaTime);
    }
    public void PedraDoChao()
    {
        this.GetComponent<Rigidbody2D>().AddForce(10*Vector2.up *velMagia * Time.deltaTime, ForceMode2D.Impulse);
     
        Destroy(this.gameObject,1.5f);
    }
    public void Onda(bool right )
    {
        
        
        if (right)
        {
            this.GetComponent<Rigidbody2D>().AddForce(20*Vector3.right * velMagia*Time.deltaTime, ForceMode2D.Impulse); ;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
            this.GetComponent<Rigidbody2D>().AddForce(20 * Vector3.left * velMagia * Time.deltaTime, ForceMode2D.Impulse);
        }
        
    }
    public void Bolha()
    {
        
       posicaoMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2);
        transform.position = Vector3.MoveTowards(this.transform.position, Camera.main.ScreenToWorldPoint(posicaoMouse),velMagia*Time.deltaTime);
        //this.transform.position = Camera.main.ScreenToWorldPoint(posicaoMouse);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Magia" && this.tipoElemento ==1 && this.tipoMagia ==2)
        {
            if (collision.GetComponent<MagiaCod>().tipoMagia ==1 && collision.GetComponent<MagiaCod>().tipoElemento != 3) {
                Debug.Log("entrou no buff");
                collision.GetComponent<MagiaCod>().velMagia *= 2;
                collision.GetComponent<MagiaCod>().dano += 2;
                collision.transform.localScale = new Vector3(2, 2, 1);
                //aplicar efeitos
            }
            if (collision.GetComponent<MagiaCod>().tipoElemento == 2 && collision.GetComponent<MagiaCod>().tipoMagia == 2)
            {
                if (collision.transform.position.x >= Player.transform.position.x)
                {
                    collision.transform.position = new Vector3(collision.transform.position.x + 3, collision.transform.position.y, collision.transform.position.z);
                }
                else if (collision.transform.position.x < Player.transform.position.x)
                {
                    collision.transform.position = new Vector3(collision.transform.position.x - 3, collision.transform.position.y, collision.transform.position.z);

                }
                collision.transform.localScale = new Vector3(6, 4, 1);
                collision.GetComponent<MagiaCod>().dano += 2;
            }
        }
        if (collision.gameObject.tag == "Inimigo" || collision.gameObject.tag == "InimigoRanged")
        {
            if ((tipoMagia ==2 && (tipoElemento ==1 || tipoElemento == 2)) || tipoElemento ==3 && tipoMagia ==1 ||(tipoElemento==4 && tipoMagia==2))
            {

            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }

    /*
    public Vector3 enPos;
    public LineRenderer lRender;
    public float vel = 50,laserLenght;
    Rigidbody2D rb;
    public int dano, tipoElemento, tipoMagia;
    public Sprite magiaArea;
    public List<Sprite> magiasEsfera = new List<Sprite>();
    // Start is called before the first frame update
    // 1 ar, 2- fogo,3 - agua, 4- terra
    void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
        
        tipoElemento = PlayerAtack.tipoElemento;
        tipoMagia = PlayerAtack.tipoMagia;

        
        
        Debug.Log("magia: "+tipoMagia+" do elemento: "+ tipoElemento);
        switch (tipoMagia)
        {
            case 1:
                {
                    //this.GetComponent<BoxCollider2D>().size = new Vector2(0.51f,0.35f); 
                    this.GetComponent<CircleCollider2D>().radius = 0.15f;
                    this.GetComponent<SpriteRenderer>().sprite = magiasEsfera[tipoElemento-1];
                    //esfera
                    dano = 4;
                    break;
                }
            case 2:
                {
                  
      
                    dano = 2;
                    this.GetComponent<SpriteRenderer>().sprite = null;

                    switch (tipoElemento)
                    {
                        case 1:
                            {

                                //ar
                                
                                lRender.SetColors(Color.white, Color.white);
                                break;

                            }
                        case 2:
                            {

                                //fogo
                                lRender.SetColors(Color.red, Color.red);
                              
                                break;
                            }
                        case 3:
                            {
                                //agua
                                lRender.SetColors(Color.blue, Color.blue);

                               
                                break;
                            }
                        case 4:
                            {

                                lRender.SetColors(Color.black, Color.black);
                                //terra
                              
                                break;
                            }

                    }
                   
                    break;
                }
            case 3:
                {
                    this.GetComponent<CircleCollider2D>().radius = 0.75f;
                    dano = 3;
                   
                    switch (tipoElemento)
                    {
                        case 1:
                            {

                                //ar
                                this.GetComponent<SpriteRenderer>().color = Color.white;
                             
                                break;

                            }
                        case 2:
                            {

                                //fogo
                                this.GetComponent<SpriteRenderer>().color = Color.red;

                                break;
                            }
                        case 3:
                            {
                                //agua
                                this.GetComponent<SpriteRenderer>().color = Color.blue;


                                break;
                            }
                        case 4:
                            {

                                this.GetComponent<SpriteRenderer>().color = Color.black;
                                //terra

                                break;
                            }

                    }
                    //area
                    this.GetComponent<SpriteRenderer>().sprite = magiaArea;
                    break;
                }


       
        

        }
       

    }

    // Update is called once per frame
    void Update()
    {
        if (tipoMagia ==2 && this.transform.position.x >= enPos.x )
        {
            Destroy(this.gameObject);
        }
    }

    public void castEsfera(Transform Pos)
    {
        this.GetComponent<Rigidbody2D>().AddForce(Pos.right * vel, ForceMode2D.Impulse);
        Destroy(this.gameObject, 3);
    }
    public void castRajada(Transform Pos)
    {

         this.GetComponent<Rigidbody2D>().AddForce(Pos.right * vel, ForceMode2D.Impulse);
        enPos = Pos.position + (transform.right*laserLenght);
        lRender.SetPositions(new Vector3[] {transform.position,enPos });
        Destroy(this.gameObject, 5);
    }
  
    public void castArea(Transform Pos)
    {
        this.transform.localScale = Vector3.one*3;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Inimigo" && tipoMagia ==1)
        {
            Destroy(this.gameObject);
        }
    }
    */
}
