﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelecaoElementos : MonoBehaviour
{

    public Material EsferaMt,PlayerMT,Magic1,Magic2;

    public Text TextoElemento;
    public int Elemento;
   
    // Start is called before the first frame update
    void Start()
    {
        TextoElemento.text = "Elemento Atual: Ar";
       
        TextoElemento.text = "Elemento Atual: Ar";
        EsferaMt.SetColor("_ColorBall", Color.white);
        PlayerMT.SetColor("_ColorPlayer", Color.white);
        Magic1.SetColor("_ColorMagic1", Color.white);
        Magic2.SetColor("_ColorMagic2", Color.white);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    // teclas para selecionar 1-4 1 ar, 2- fogo,3 - agua, 4- terra
    public void OnMouseOver()
    {
        SetText(Elemento);
        PlayerAtack.tipoElemento = Elemento;
       
    }
    public void SetText(int elemento)
    {
        switch (elemento)
        {
            case 1:
                {

                    TextoElemento.text = "Elemento Atual: Ar";
                    EsferaMt.SetColor("_ColorBall", Color.white);
                    PlayerMT.SetColor("_ColorPlayer", Color.white);
                    Magic1.SetColor("_ColorMagic1", Color.white);
                    Magic2.SetColor("_ColorMagic2", Color.white);
                    break;
                }
            case 2:
                {
                    TextoElemento.text = "Elemento Atual: Fogo";
                    EsferaMt.SetColor("_ColorBall", new Color(3.441591f, 2.70282f, 0, 1));
                    PlayerMT.SetColor("_ColorPlayer", new Color(3.441591f, 2.70282f, 0, 1));
                    Magic1.SetColor("_ColorMagic1", new Color(3.441591f, 2.70282f, 0, 1));
                    Magic2.SetColor("_ColorMagic2", new Color(0.5676507f, 0.445799f, 0, 0.4941176f));
                    break;
                }
            case 3:
                {
                    TextoElemento.text = "Elemento Atual: Agua";
                    Magic1.SetColor("_ColorMagic1", Color.cyan);
                    Magic2.SetColor("_ColorMagic2", Color.cyan);
                    EsferaMt.SetColor("_ColorBall", Color.cyan);
                    PlayerMT.SetColor("_ColorPlayer", Color.cyan);
                    break;
                }
            case 4:
                {
                    TextoElemento.text = "Elemento Atual: Terra";
                    Magic1.SetColor("_ColorMagic1", Color.black);
                    Magic2.SetColor("_ColorMagic2", Color.black);
                    /*
                    Magic1.SetColor("_ColorMagic1", new Color(1, 0.6f, 0, 1));
                    Magic2.SetColor("_ColorMagic2", new Color(1, 0.6f, 0, 1));
                    */
                    EsferaMt.SetColor("_ColorBall", new Color(1, 0.6f, 0, 1));
                    PlayerMT.SetColor("_ColorPlayer", new Color(1, 0.6f, 0, 1));
                    break;
                }

        }
    }
}
   