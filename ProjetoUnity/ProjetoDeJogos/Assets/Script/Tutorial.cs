﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public bool Entrou;
    public float TempoAtual;
    public Image ImagemTutorial;
    public Sprite TelaDoTutorial;
    public BoxCollider2D bx;
    // Start is called before the first frame update
    void Start()
    {
        Entrou = false;
        ImagemTutorial.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (TempoAtual >= 0 && ImagemTutorial.enabled && Entrou)
        {
            TempoAtual -= Time.deltaTime;
        }
        if (ImagemTutorial.enabled && TempoAtual<=0 )
        {
            ImagemTutorial.enabled = false;
            Destroy(this.gameObject,1f);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Entrou = true;
            ImagemTutorial.sprite = TelaDoTutorial;
            ImagemTutorial.enabled = true;
        }
    }


}
