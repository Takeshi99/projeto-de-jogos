﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FimGameTeste : MonoBehaviour
{
    public GameObject PainelMenu;
    public Image ImagemFimDoJogo;
    public Sprite ImagemFim;
    public List<GameObject> Inimigos;
    public List<GameObject> InimigosRanged ;
    public Text TextoMenu;

    // Start is called before the first frame update
    void Start()
    {
        PainelMenu.SetActive(false);
        ImagemFimDoJogo.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i =0; i<Inimigos.Count;i++)
        {
            if (Inimigos[i].GetComponent<EnemyBehavior>().vida <=0)
            {
                Inimigos.RemoveAt(i);
            }
        }
        for (int i = 0; i < InimigosRanged.Count; i++)
        {
            if (InimigosRanged[i].GetComponent<EnemyRanged>().vida <= 0)
            {
                InimigosRanged.RemoveAt(i);
            }
        }
        if (Inimigos.Count<=0 && InimigosRanged.Count<=0)
        {
            TextoMenu.text = "";
            PainelMenu.active = true;
          
            Time.timeScale = 0f;
            ImagemFimDoJogo.sprite = ImagemFim;
            ImagemFimDoJogo.enabled = true;
        }
    }
}
