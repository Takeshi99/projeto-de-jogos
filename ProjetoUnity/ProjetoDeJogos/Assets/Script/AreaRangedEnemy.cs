﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaRangedEnemy : MonoBehaviour
{
    public GameObject InimigoAtrelado;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" )
        {
            InimigoAtrelado.GetComponent<EnemyRanged>().PosParaArremecar = collision.gameObject.transform.position;
        }
    }
    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            InimigoAtrelado.GetComponent<EnemyRanged>().PosParaArremecar = collision.gameObject.transform.position;
        }
    }
    
}
