﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMoviment : MonoBehaviour
{
    public Animator Animin;
    public GameObject PainelMorte;
    public Text TextoVida;
    public float tempoParaPular, tempoPulando,tempoParaDash,TempoDash,dashDirection,dashforce,cdDash,TempoEntreDash;
    public float forcaPulo;
    public float x;
    public float VelOriginal = 50;
    public float VelAtual;
    public int vida = 5;
    public static bool isGrounded, isCasting, isJumping,isMoving,pulouComMagia,isDashing,TakeDamage;
    public BoxCollider2D bx = new BoxCollider2D();
    public Rigidbody2D rb = new Rigidbody2D();
    // Start is called before the first frame update
    void Start()
    {
        TempoEntreDash = 1f;
        TakeDamage = true;
        dashforce = 20f;
        tempoParaDash = 0.25f;
        VelAtual = VelOriginal;
        PainelMorte.active = false;
        SetVida();
        rb = GetComponent<Rigidbody2D>();
        bx = GetComponent<BoxCollider2D>();
        
        
    }

    // Update is called once per frame
    void Update()
    {

        if (cdDash>=0)
        {
            cdDash -= Time.deltaTime;
        }
        //dash
        if (Input.GetKeyDown(KeyCode.LeftShift) && x != 0 && !isDashing && cdDash <=0)
        {
            cdDash = TempoEntreDash;   
            isDashing = true;
            dashDirection = (int)x;
            TempoDash = tempoParaDash;
            rb.velocity = Vector2.zero;
        }
        if (isDashing)
        {
            TakeDamage = false;
            rb.gravityScale = 0;
            this.GetComponent<CircleCollider2D>().isTrigger = true;
            Debug.Log("Entrou dash");
            rb.velocity =  new Vector2( dashDirection * dashforce ,rb.velocity.y);
            TempoDash -= Time.deltaTime;
            
            if (TempoDash <=0)
            {
                TakeDamage = true;
                rb.gravityScale = 5;
                this.GetComponent<CircleCollider2D>().isTrigger = false;
                isDashing = false;
            }

        }

        Pulo();
        if (vida<=0)
        {
            vida = 0;
            Time.timeScale = 0f;
            PainelMorte.active = true;
            //GameOver

        }
    }
    private void FixedUpdate()
    {
        Movimento();
    }

    public void SetVida()
    {
        TextoVida.text = "Vida: "+ vida;
    }
  

    public void Movimento()
    {
        if (PlayerAtack.tipoElemento == 1)
        {
            VelAtual = VelOriginal * 2f;
        }
        else
        {
            VelAtual = VelOriginal;
        }

        x = Input.GetAxisRaw("Horizontal");
        Animin.SetFloat("Vel", Mathf.Abs(x));
        if (x == 0)
        {
            isMoving = false;
        }
        else
        {
            isMoving = true;
        }
        if (x > 0 && !isCasting)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
        else if(x<0 && !isCasting)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        if (!isCasting && !isDashing)
        {
            Animin.SetFloat("Vel",Mathf.Abs(x));
            rb.velocity = new Vector2(x * VelAtual * Time.deltaTime, rb.velocity.y);
        }
        else
        {
           // rb.velocity = Vector2.zero;
        }
    }
    public void Pulo()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.velocity = Vector2.up * forcaPulo;
            isJumping = true;
            tempoPulando = tempoParaPular;
        }
             if (Input.GetKey(KeyCode.Space) && isJumping)
             {
                
             
                if (tempoPulando > 0)
                {
                    rb.velocity = Vector2.up * forcaPulo;
                    tempoPulando -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;
                }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Inimigo") && TakeDamage)
        {

            vida -= collision.GetComponent<EnemyBehavior>().dano;
            SetVida();
        } 

        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
            pulouComMagia = false;
        }
        if (collision.gameObject.tag == "KillZone")
        {
            vida = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
            pulouComMagia = false;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
}
