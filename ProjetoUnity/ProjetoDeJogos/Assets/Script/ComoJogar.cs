﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComoJogar : MonoBehaviour
{
    public Button Botao;
    public Text Texto;
    public GameObject controles, Elementos;
    // Start is called before the first frame update
    void Start()
    {
        Texto.text = "Aperte barra de espaço proxima instrução.";
        Botao.gameObject.SetActive(false);
        controles.SetActive(true);
        Elementos.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Botao.gameObject.SetActive(true);
            Texto.text = "Aperte o botao para iniciar";
            controles.SetActive(false);
            Elementos.SetActive(true);

        }
    }
}
