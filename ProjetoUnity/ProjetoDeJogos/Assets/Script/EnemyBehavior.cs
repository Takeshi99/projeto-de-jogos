﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyBehavior : MonoBehaviour
{

    private GameObject Player;
    public bool isSlow, isStun, isFire, canWalk, CanSeePlayer;
    public float TempoAtual, TempoAtualAgua, TempoAtualFogo, TempoAtualPedra, TempoDeccAgua, TempoDeccFogo, TempoDeccPedra, TempoDecc, tempoAndar, tempoAtualAndar, randDirection;
    public GameObject TextoEfeito;
    public int vida = 50, dano, segundos, segundosAnterior;
    public float velAtual, VelOriginal;


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //Movimentacao aleatoria
        if (tempoAtualAndar >= 0)
        {
            tempoAtualAndar -= Time.deltaTime;
        }
        else
        {
            tempoAtualAndar = tempoAndar;
            randDirection = Random.Range(-1, 2);
        }
        //              efeitos dos elemtos
        if (!isStun) {
            MovimentacaoInimigo();
        }
        if (TempoAtual < TempoDecc - 0.2 && !isFire)
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
        }

        segundos = (int)TempoAtualFogo % 60;
        if (segundos != segundosAnterior)
        {

            if (isFire)
            {
                if (segundos % 2 == 0)
                {
                    this.GetComponent<SpriteRenderer>().color = Color.white;
                }
                else
                {
                    this.GetComponent<SpriteRenderer>().color = Color.red;
                }
                segundosAnterior = segundos;
                Debug.Log("passou um segundo");

                vida -= 1;
            }

        }
        if (TempoAtualPedra > 0)
        {
            TempoAtualPedra -= Time.deltaTime;

        } else if (TempoAtualPedra <= 0)
        {
            // TextoEfeito.GetComponent<TextMeshPro>().text = " ";
            isStun = false;
        }
        if (TempoAtualAgua > 0)
        {
            //  TextoEfeito.GetComponent<TextMeshPro>().text = " ";
            TempoAtualAgua -= Time.deltaTime;

        }
        else if (TempoAtualAgua <= 0)
        {
            // TextoEfeito.GetComponent<TextMeshPro>().text = " ";
            isSlow = false;
            velAtual = VelOriginal;
        }

        if (TempoAtualFogo > 0)
        {
            //TextoEfeito.GetComponent<TextMeshPro>().text = " ";
            TempoAtualFogo -= Time.deltaTime;

        }
        else if (TempoAtualFogo <= 0)
        {
            //  TextoEfeito.GetComponent<TextMeshPro>().text = " ";
            isFire = false;
        }

        if (TempoAtual > 0)
        {
            TempoAtual -= Time.deltaTime;

        }
        else
        {
            TextoEfeito.GetComponent<TextMeshPro>().text = " ";

        }
        //     fim efeitos elementos
        if (vida <= 0)
        {
            Destroy(this.gameObject, 0.5f);
        }
    }

    public void tomaDano(int dano)
    {
        vida -= dano;
    }
    public void MovimentacaoInimigo()
    {
        if (CanSeePlayer) {
            if (Player.transform.position.x > this.transform.position.x)
            {

                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
            transform.position = Vector2.MoveTowards(this.transform.position, Player.transform.position, velAtual * Time.deltaTime);
        }
        else
        {
            if (randDirection <= 0)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;

            }
            else
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(randDirection * velAtual, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "KillZone")
        {
            vida = 0;
        }
        if (collision.gameObject.tag == "Magia" && this.gameObject.tag == "Inimigo")
        {
            CanSeePlayer = true;
            tomaDano(collision.gameObject.GetComponent<MagiaCod>().dano);

            switch (collision.gameObject.GetComponent<MagiaCod>().tipoElemento)
            {
                case 1:
                    {
                        TempoAtual = TempoDecc;
                        //ar
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Knock Back";
                        if (Player.transform.position.x > this.transform.position.x) {

                            this.GetComponent<Rigidbody2D>().AddForce(Vector2.left * 100);
                        } else
                        {
                            this.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 100);
                        }
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        segundosAnterior = segundos + 1;
                        break;

                    }
                case 2:
                    {
                        TempoAtual = TempoDecc;
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Fire Damage";
                        isFire = true;
                        TempoAtualFogo = TempoDeccFogo;
                        //fogo
                        segundosAnterior = segundos + 1;
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    }
                case 3:
                    {
                        TempoAtual = TempoDecc;
                        //agua
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Slow Down";
                        isSlow = true;
                        TempoAtualAgua = TempoDeccAgua;
                        velAtual = velAtual / 2;
                        segundosAnterior = segundos + 1;
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        break;

                    }
                case 4:
                    {
                        if (collision.gameObject.GetComponent<MagiaCod>().tipoMagia == 2)
                        {
                            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 200);

                        }
                        else
                        {
                            isStun = true;
                            TextoEfeito.GetComponent<TextMeshPro>().text = " Stun";
                        }
                        TempoAtual = TempoDecc - 0.5f;

                        TempoAtualPedra = TempoDeccPedra -0.5f;
                        segundosAnterior = segundos + 1;
                        //terra
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    }

            }
        }

    }
}
    /*
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Magia")
        {
            tomaDano(collision.gameObject.GetComponent<MagiaCod>().dano -1);

            switch (collision.gameObject.GetComponent<MagiaCod>().tipoElemento)
            {
                case 1:
                    {
                        TempoAtual = TempoDecc;
                        //ar
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Knock Back";
                        if (Player.transform.position.x > this.transform.position.x)
                        {

                            this.GetComponent<Rigidbody2D>().AddForce(Vector2.left * 200);
                        }
                        else
                        {
                            this.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
                        }
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        segundosAnterior = segundos + 1;
                        break;

                    }
                case 2:
                    {
                        TempoAtual = TempoDecc;
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Fire Damage";
                        isFire = true;
                        TempoAtualFogo = TempoDeccFogo;
                        //fogo
                        segundosAnterior = segundos + 1;
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    }
                case 3:
                    {
                        TempoAtual = TempoDecc;
                        //agua
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Slow Down";
                        isSlow = true;
                        TempoAtualAgua = TempoDeccAgua;
                        velAtual = velAtual / 2;
                        segundosAnterior = segundos + 1;
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        break;

                    }
                case 4:
                    {
                        TempoAtual = TempoDecc;
                        isStun = true;
                        TextoEfeito.GetComponent<TextMeshPro>().text = " Stun";
                        TempoAtualPedra = TempoDeccPedra;
                        segundosAnterior = segundos + 1;
                        //terra
                        this.GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    }

            }
        }
    }
    */

