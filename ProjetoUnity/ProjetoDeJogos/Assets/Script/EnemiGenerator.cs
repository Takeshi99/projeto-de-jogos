﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiGenerator : MonoBehaviour
{
    public GameObject Inimigos;
    public float TempoAtual, TempoMin,TempoMax;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (TempoAtual <= 0)
        {
            GerarInimigos();
            TempoAtual = Random.Range(TempoMin,TempoMax);
        }
        else
        {
            TempoAtual -= Time.deltaTime;
        }

    }
    public void GerarInimigos ()
    {
        GameObject a = Instantiate(Inimigos,this.transform.position,Quaternion.identity);
    }
}
