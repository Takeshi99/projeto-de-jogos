﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAtack : MonoBehaviour
{
    
    public float timeAnimaion;
    public Animator Animin;
    // public Text TextoElemento;
    public Text TextoCd, TextoMagia;
    public bool podeCastar;
    public LineRenderer lr;
    public GameObject objMira, objMagia, mao;
    // Start is called before the first frame update
    public Vector3 posicaoMouse;
    public float tempoParaSelecionarElemento = 0.2f, tempoParaSelecionarMagia = 0.2f, tempoAtualElemento, tempoAtualMagia, tempoParaCastar, tempoAtualParaCastar, tempoRaio, tempoAtualraio;
    static public int tipoMagia = 1, tipoElemento = 1;
    // mudar tipo de feitico esfera q, rajada e, area r
    // teclas para selecionar 1-4 1 ar, 2- fogo,3 - agua, 4- terra
    // Start is called before the first frame update
    void Start()
    {
        timeAnimaion = 0.2f;
        TextoMagia.text = "Magia Atual: Esfera";
        tipoMagia = 1;
        tipoElemento = 1;
        podeCastar = true;
        //lr.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
           
        }
        if (Input.GetMouseButtonDown(0))
        {
            PlayerMoviment.isCasting = true;
            FLipAtack();
        }
      
        SetCd();
        //selecionaElemento();
        selecionaTipoMagia();
        mira();
        atira();
    }
    private void FixedUpdate()
    {

    }

    public void SetCd()
    {
        TextoCd.text = string.Format("Tempo para Magia: {0:0.00}", tempoAtualParaCastar);
    }
    public void FLipAtack()
    {
        if (objMira.transform.position.x >= this.gameObject.transform.position.x)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        
    }
    public void selecionaTipoMagia()
    {
        if (Input.GetKeyDown(KeyCode.Q) && tempoAtualMagia <= 0)
        {
            TextoMagia.text = "Magia Atual: Esfera";

            tempoAtualMagia = tempoParaSelecionarMagia;
            tipoMagia = 1;
        }
        if (Input.GetKeyDown(KeyCode.W) && tempoAtualMagia <= 0)
        {
            TextoMagia.text = "Magia Atual: Laser";

            tempoAtualMagia = tempoParaSelecionarMagia;
            tipoMagia = 2;
        }
        if (Input.GetKeyDown(KeyCode.E) && tempoAtualMagia <= 0)
        {
            TextoMagia.text = "Magia Atual: Area";
            tempoAtualMagia = tempoParaSelecionarMagia;
            tipoMagia = 3;
        }
        if (tempoAtualMagia > 0)
        {
            tempoAtualMagia -= Time.deltaTime;
        }
    }
    /*
    public void selecionaElemento()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && tempoAtualElemento <= 0)
        {
            TextoElemento.text = "Elemento Atual: Ar";
            tempoAtualElemento = tempoParaSelecionarElemento;
            tipoElemento = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && tempoAtualElemento <= 0)
        {
            TextoElemento.text = "Elemento Atual: Fogo";
            tempoAtualElemento = tempoParaSelecionarElemento;
            tipoElemento = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && tempoAtualElemento <= 0)
        {
            TextoElemento.text = "Elemento Atual: Agua";
            tempoAtualElemento = tempoParaSelecionarElemento;
            tipoElemento = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && tempoAtualElemento <= 0)
        {
            TextoElemento.text = "Elemento Atual: Terra";
            tempoAtualElemento = tempoParaSelecionarElemento;
            tipoElemento = 4;
        }
        if (tempoAtualElemento > 0)
        {
            tempoAtualElemento -= Time.deltaTime;
        }
    }
    */
    public void mira()
    {

        posicaoMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2);
        objMira.transform.position = Camera.main.ScreenToWorldPoint(posicaoMouse);
    }
    public void atira()
    {
        if (tipoMagia == 2 && (tempoAtualraio <= 0 || Input.GetMouseButtonUp(0)))
        {
            tempoAtualraio = -1;
          //  lr.enabled = false;
        }

        if (tipoMagia != 2 || PlayerMoviment.isMoving == true)
        {
           // lr.enabled = false;
        }

        if (Input.GetMouseButtonDown(0) && tempoAtualParaCastar <= 0)
        {
            Animin.SetBool("Casting", true);
            if (tipoMagia == 1)
            {

                if (tipoElemento == 1)
                {
                    timeAnimaion = 0.2f;
                    GameObject a = Instantiate(objMagia, mao.transform.position, mao.transform.rotation);
                    a.GetComponent<MagiaCod>().EsferaAr(mao.transform);
                    tempoAtualParaCastar = tempoParaCastar ;
                    
                    Destroy(a, 3f);
                    if (objMira.transform.position.y <= this.gameObject.transform.position.y - 1 && !PlayerMoviment.pulouComMagia)
                    {

                        Debug.Log("Entrou no pulo");
                        PlayerMoviment.pulouComMagia = true;
                        a.GetComponent<MagiaCod>().ImpulsoAr();

                    }
                }
                if (tipoElemento == 2)
                {
                    // 8 magic numer
                    GameObject a = Instantiate(objMagia, new Vector3(this.gameObject.transform.position.x +4, this.gameObject.transform.position.y, this.gameObject.transform.position.z), Quaternion.identity);
                    a.transform.parent = this.transform;
                    tempoAtualParaCastar = tempoParaCastar;
       
                    Destroy(a, 6f);
                }
                if (tipoElemento == 3 && PlayerMoviment.isGrounded)
                {

                    if (objMira.transform.position.x >= this.transform.position.x)
                    {
                        GameObject a = Instantiate(objMagia, new Vector3(mao.transform.position.x, mao.transform.position.y-0.5f, 0), Quaternion.identity);
                        a.GetComponent<MagiaCod>().Onda(true);
                     
                        Destroy(a, 2);
                    }
                    else if (objMira.transform.position.x < this.transform.position.x)
                    {
                        GameObject a = Instantiate(objMagia, new Vector3(mao.transform.position.x,mao.transform.position.y -0.5f,0), Quaternion.identity); ;
                        a.GetComponent<MagiaCod>().Onda(false);
                      
                        Destroy(a, 5);
                    }
                    
                    tempoAtualParaCastar = tempoParaCastar + 1f;
                  
                }
                if (tipoElemento ==4)
                {
                    GameObject a = Instantiate(objMagia, mao.transform.position, Quaternion.identity);
                    a.GetComponent<MagiaCod>().Pedrada(Camera.main.ScreenToWorldPoint(posicaoMouse));
                  
                    tempoAtualParaCastar = tempoParaCastar + 0.2f;
                    Destroy(a, 5);
                }


            }
            if (tipoMagia == 2)
            {
                timeAnimaion = 0.8f;
                if (tipoElemento == 1)
                {
                    GameObject a = Instantiate(objMagia, objMira.transform.position, objMira.transform.rotation);
                    tempoAtualraio = tempoRaio;
                    tempoAtualParaCastar = tempoParaCastar + 0.5f;

                }
                if (tipoElemento == 2 && PlayerMoviment.isGrounded)
                {
                    if (objMira.transform.position.x >= this.transform.position.x)
                    {
                        GameObject a = Instantiate(objMagia, new Vector3(mao.transform.position.x + 2, mao.transform.position.y, mao.transform.position.z), Quaternion.identity);
                        a.GetComponent<SpriteRenderer>().flipX = false;
                        a.GetComponent<MagiaCod>().SoproDoDragao();
                    }
                    else if (objMira.transform.position.x < this.transform.position.x)
                    {
                        GameObject a = Instantiate(objMagia, new Vector3(mao.transform.position.x - 2, mao.transform.position.y, mao.transform.position.z), Quaternion.identity);
                        a.GetComponent<SpriteRenderer>().flipX = true;
                        a.GetComponent<MagiaCod>().SoproDoDragao();
                    }
                    tempoAtualParaCastar = tempoParaCastar + 0.5f;
                    this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    PlayerMoviment.isCasting = true;
                }
                if (tipoElemento == 3 && PlayerMoviment.isGrounded)
                {
                    
                    GameObject a = Instantiate(objMagia, mao.transform.position, objMira.transform.rotation);

                    tempoAtualParaCastar = tempoParaCastar + 0.5f;
                }
                if (tipoElemento ==4 && PlayerMoviment.isGrounded)
                {

                    Vector3 posChao = Camera.main.ScreenToWorldPoint(posicaoMouse);
                    GameObject a = Instantiate(objMagia, new Vector3(posChao.x,this.transform.position.y-2,0),Quaternion.identity);
                    a.GetComponent<MagiaCod>().PedraDoChao();
                    tempoAtualParaCastar = tempoParaCastar + 0.5f;
                }


            }
            /*
            Debug.Log("atirou magia 2");
            GameObject a = Instantiate(objMagia, mao.transform.position, mao.transform.rotation);

            if (Input.GetMouseButtonUp(0) && tempoRaio <= 0)
            {
                lr.enabled = false;
                tempoRaio = 2f;

            }
            else
            {

                a.GetComponent<MagiaCod>().castRajada(mao.transform);
            }

            if (tempoRaio > 0)
            {
                tempoRaio -= Time.deltaTime;
            }
            tempoAtualParaCastar = tempoParaCastar;
           */

        }
        if (tempoAtualParaCastar <= timeAnimaion)
        {
            PlayerMoviment.isCasting = false;
            Animin.SetBool("Casting", false);
        }
        if (tempoAtualParaCastar > 0)
        {
            
            podeCastar = false;
            tempoAtualParaCastar -= Time.deltaTime;
        }
        else
        {
            podeCastar = true;
        }
        if (tempoAtualraio > 0)
        {
            tempoAtualraio -= Time.deltaTime;
        }
    }
}
