﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void onClickIniciar()
    {
       
        SceneManager.LoadScene("Tutorial");
    }
    public void onClickSair()
    {
        Application.Quit();
    }
    public void onClickComoJogar()
    {
        SceneManager.LoadScene("ComoJogar");
    }
}
