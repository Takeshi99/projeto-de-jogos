﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaVisiaoInimigo : MonoBehaviour
{
    public GameObject InimigoAtrelado;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (InimigoAtrelado.gameObject.tag== "InimigoRanged")
            {
                InimigoAtrelado.GetComponent<EnemyRanged>().CanSeePlayer = true;
            }
            else
            {
                InimigoAtrelado.GetComponent<EnemyBehavior>().CanSeePlayer = true;
            }
            
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (InimigoAtrelado.tag == "InimigoRanged")
            {
                InimigoAtrelado.GetComponent<EnemyRanged>().CanSeePlayer = false;
            }
            else
            {
                InimigoAtrelado.GetComponent<EnemyBehavior>().CanSeePlayer = false;
            }

        }
    }
}
