﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjArremeca : MonoBehaviour
{
    public float dano, vel;
    public Vector3 posParaIr;
    // Start is called before the first frame update
    void Start()
    {
        dano = 5;
        vel = 10;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position,posParaIr,Time.deltaTime*vel);
        if (this.transform.position == posParaIr)
        {
            Destroy(this.gameObject,0.5f);
        }
    }
    public void setpos(Vector3 Pos)
    {
        posParaIr = Pos;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && PlayerMoviment.TakeDamage)
        {
            collision.gameObject.GetComponent<PlayerMoviment>().vida -= (int)dano;
            collision.gameObject.GetComponent<PlayerMoviment>().SetVida();
            Destroy(this.gameObject);
        }
    }
}
