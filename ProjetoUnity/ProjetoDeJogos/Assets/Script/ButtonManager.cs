﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public GameObject CanvasElemento;
    public GameObject PainelMenu;
    public GameObject player;
    public Text TextoMenu;
    public bool Apertou;
    // Start is called before the first frame update
    void Start()
    {
        CanvasElemento.SetActive(false);
        Time.timeScale = 1f;
        
        Apertou = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {

            Time.timeScale = 0.5f;
            CanvasElemento.SetActive(true);



        }

        if (Input.GetMouseButtonUp(1))
        {
            Time.timeScale = 1f;
            CanvasElemento.SetActive(false); 
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !Apertou && player.GetComponent<PlayerMoviment>().vida > 0)
        {
            TextoMenu.text = "Game pause";
            Apertou = true;
            Time.timeScale = 0f;
            PainelMenu.active = true;
            Debug.Log("abertou menu");
        }else if (Input.GetKeyDown(KeyCode.Escape) && Apertou && player.GetComponent<PlayerMoviment>().vida > 0)
        {
            TextoMenu.text = "Game Over";
            Apertou = false;
            Time.timeScale = 1f;
            PainelMenu.active = false;
            Debug.Log("fechou menu");
        }
    }
    public void onClickMenu()
    {
        Debug.Log("esta apertando no botao");
        SceneManager.LoadScene("MenuInicial");
    }
  
}
