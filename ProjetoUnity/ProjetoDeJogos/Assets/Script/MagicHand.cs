﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicHand : MonoBehaviour
{
    public Vector3 diff;
    public float vel=10f;
    public Transform mira;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
        /*
        Vector3 diff = mira.position - transform.position;
        float rot_z = (Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg );
        Quaternion rot = Quaternion.AngleAxis(rot_z,Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation,rot,vel*Time.deltaTime);
        */
    }
}
