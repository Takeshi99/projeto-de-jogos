﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FimDoTutorial : MonoBehaviour
{
    public GameObject Inimigo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Inimigo.GetComponent<EnemyBehavior>().vida <=0)
        {
            SceneManager.LoadScene("GameTeste");
            Debug.Log("Acabou");
            Destroy(this.gameObject,5f);
        }
    }
}
